class Global::ChinazoUser < ActiveRecord::Base

	validates_presence_of :username, :gender, :cedula, :email, :created_by
	validates :cedula, numericality: true
	validates :cedula, :email, uniqueness: true

	scope :active,->{
		where active: true
	}

	scope :inactive,->{
		where active: false
	}	

	scope :status,->(status){  
		where active: status
	}

	has_many :chinazo_comments # todos los has_many van en plural la relacion

	before_create :audit

	def audit
		begin #manejo de transacciones completas
			self.created_by = 1
		rescue => e # captura excepciones
			puts e
		end
	end

end
