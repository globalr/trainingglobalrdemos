# para agregar un migration. en la consola rails g migration NombreMigration
class CreateGlobalChinazoUsers < ActiveRecord::Migration
  def change
    create_table :global_chinazo_users do |t|

      t.string  :username, null: false
      t.string  :sex, limit: 1, null: false
      t.integer :cedula, unique: true, null: false
      t.string  :email, unique: true, null: false
      t.boolean :active, default: true
      t.integer :created_by, null: false
      t.integer :updated_by
      t.timestamps
    end
  end
end
